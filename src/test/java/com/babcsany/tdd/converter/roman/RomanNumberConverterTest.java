package com.babcsany.tdd.converter.roman;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by peter on 2016. 04. 30..
 */
public class RomanNumberConverterTest {
    @Test
    public void should_convert_numbers_to_roman_correctly() throws Exception {
        RomanNumberConverter romanNumberConverter = new RomanNumberConverter();
        Assert.assertEquals("I", romanNumberConverter.convert(1));
        Assert.assertEquals("II", romanNumberConverter.convert(2));
        Assert.assertEquals("III", romanNumberConverter.convert(3));
        Assert.assertEquals("V", romanNumberConverter.convert(5));
        Assert.assertEquals("VIII", romanNumberConverter.convert(8));
        Assert.assertEquals("X", romanNumberConverter.convert(10));
        Assert.assertEquals("XVIII", romanNumberConverter.convert(18));
        Assert.assertEquals("XX", romanNumberConverter.convert(20));
        Assert.assertEquals("IV", romanNumberConverter.convert(4));
        Assert.assertEquals("IX", romanNumberConverter.convert(9));
        Assert.assertEquals("XLIV", romanNumberConverter.convert(44));
        Assert.assertEquals("L", romanNumberConverter.convert(50));
        Assert.assertEquals("CCCXCIX", romanNumberConverter.convert(399));
        Assert.assertEquals("MCDXLIX", romanNumberConverter.convert(1449));
        Assert.assertEquals("MMMCMXCIV", romanNumberConverter.convert(3994));
        Assert.assertEquals("MMMCMXCIX", romanNumberConverter.convert(3999));
    }
}
