package com.babcsany.tdd.converter.roman;

/**
 * Created by peter on 2016. 04. 30..
 */
public class RomanNumberConverter {
    private enum RomanDigit {
        M(1000),
        CM(900), D(500), CD(400), C(100),
        XC(90),  L(50),  XL(40),  X(10),
        IX(9),   V(5),   IV(4),   I(1);
        private int value;

        RomanDigit(int value) {
            this.value = value;
        }
    }

    public String convert(int num) {
        String result = "";
        int i;

        while (num > 0) {
            for (RomanDigit romanDigit : RomanDigit.values()) {
                if (num >= romanDigit.value) {
                    result = result.concat(romanDigit.name());
                    num -= romanDigit.value;
                    break;
                }
            }
        }

        return result;
    }
}
